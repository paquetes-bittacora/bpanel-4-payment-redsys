<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Redsys\Tests\Unit;

use Bittacora\Bpanel4\Payment\Redsys\Services\BpanelRedsysNotificationService;
use Bittacora\Bpanel4\Payment\Redsys\Tests\Unit\MockFactories\MarkOrderAsPaidMockFactory;
use Bittacora\Bpanel4\Payment\Redsys\Tests\Unit\MockFactories\MarkOrderAsPaymentErrorMockFactory;
use Bittacora\Laravel\Redsys\Dtos\TpvNotificationResult;
use Bittacora\Laravel\Redsys\Services\TpvNotificationService;
use Illuminate\Http\Request;
use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Sermepa\Tpv\TpvException;

final class BpanelRedsysNotificationServiceTest extends TestCase
{
    use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

    /**
     * @throws TpvException
     */
    public function testLlamaALaAccionQueMarcaElPedidoComoPagadoSilaRespuestaDelTpvEsCorrecta(): void
    {
        $request = $this->getRequestMock();
        $laravelTpvNotificationServiceMock = Mockery::mock(TpvNotificationService::class);
        $laravelTpvNotificationServiceMock->shouldReceive('processNotification')
            ->andReturn(new TpvNotificationResult(true, 0, 1));

        $notificationService = new BpanelRedsysNotificationService(
            $laravelTpvNotificationServiceMock,
            MarkOrderAsPaidMockFactory::shouldBeCalled(),
            MarkOrderAsPaymentErrorMockFactory::shouldNotBeCalled(),
        );
        $notificationService->processNotification($request);
    }

    /**
     * @throws TpvException
     */
    public function testLlamaAlaAccionQueMarcaElPedidoComoIncorrectoSiLaRespuestaDelTpvEsIncorrecta(): void
    {
        $request = $this->getRequestMock();
        $laravelTpvNotificationServiceMock = Mockery::mock(TpvNotificationService::class);
        $laravelTpvNotificationServiceMock->shouldReceive('processNotification')
            ->andReturn(new TpvNotificationResult(false, 0, 1));

        $notificationService = new BpanelRedsysNotificationService(
            $laravelTpvNotificationServiceMock,
            MarkOrderAsPaidMockFactory::shouldNotBeCalled(),
            MarkOrderAsPaymentErrorMockFactory::shouldBeCalled(),
        );
        $notificationService->processNotification($request);
    }

    private function getRequestMock(): Request&MockInterface
    {
        $mock = Mockery::mock(Request::class);
        $mock->shouldIgnoreMissing();
        return $mock;
    }
}
