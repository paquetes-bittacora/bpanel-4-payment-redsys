<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Redsys\Tests\Unit\MockFactories;

use Bittacora\Bpanel4\Payment\Redsys\Actions\MarkOrderAsPaymentError;
use Mockery;
use Mockery\Mock;

final class MarkOrderAsPaymentErrorMockFactory
{
    /**
     * @return Mock&MarkOrderAsPaymentError
     */
    public static function shouldBeCalled()
    {
        $mock = Mockery::mock(MarkOrderAsPaymentError::class);
        $mock->shouldReceive('handle')->once();
        return $mock;
    }

    /**
     * @return Mock&MarkOrderAsPaymentError
     */
    public static function shouldNotBeCalled()
    {
        $mock = Mockery::mock(MarkOrderAsPaymentError::class);
        $mock->shouldReceive('handle')->never();
        return $mock;
    }
}
