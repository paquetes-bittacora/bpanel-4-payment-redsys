<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Redsys\Tests\Unit\MockFactories;

use Bittacora\Bpanel4\Payment\Redsys\Actions\MarkOrderAsPaid;
use Mockery;
use Mockery\Mock;

final class MarkOrderAsPaidMockFactory
{
    /**
     * @return Mock&MarkOrderAsPaid
     */
    public static function shouldBeCalled()
    {
        $mock = Mockery::mock(MarkOrderAsPaid::class);
        $mock->shouldReceive('handle')->once();
        return $mock;
    }

    /**
     * @return Mock&MarkOrderAsPaid
     */
    public static function shouldNotBeCalled()
    {
        $mock = Mockery::mock(MarkOrderAsPaid::class);
        $mock->shouldReceive('handle')->never();
        return $mock;
    }
}
