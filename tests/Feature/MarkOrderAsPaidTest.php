<?php

namespace Bittacora\Bpanel4\Payment\Redsys\Tests\Feature;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Payment\Redsys\Actions\MarkOrderAsPaid;
use Bittacora\Bpanel4\Payment\Redsys\Mail\OrderPaidAdminMail;
use Bittacora\Bpanel4\Payment\Redsys\Mail\OrderPaidClientMail;
use Bittacora\Laravel\Redsys\Dtos\TpvNotificationResult;
use Bittacora\Laravel\Redsys\Services\OrderNumberFormatter;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

final class MarkOrderAsPaidTest extends TestCase
{
    use RefreshDatabase;

    private MarkOrderAsPaid $markOrderAsPaid;
    private TpvNotificationResult $notificationResult;
    private Order $order;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setNotificationResult();
        Mail::fake();
        $this->markOrderAsPaid = $this->app->make(MarkOrderAsPaid::class);
        $this->artisan('bpanel4-orders:create-default-order-statuses');
    }

    public function testMarcaElPedidoComoPagado(): void
    {
        $this->triggerAction();
        self::assertEquals(OrderStatus::PAYMENT_COMPLETED->value, $this->order->getStatus()->id);
    }

    public function testEnviaUnEmailAlAdministrador(): void
    {
        $this->triggerAction();
        Mail::assertSent(OrderPaidAdminMail::class);
    }

    public function testEnviaUnEmailAlCliente(): void
    {
        $this->triggerAction();
        Mail::assertSent(OrderPaidClientMail::class);
    }

    private function setNotificationResult(): void
    {
        $this->order = (new OrderFactory())->getFullOrder();

        $this->notificationResult = new TpvNotificationResult(
            true,
            0,
            OrderNumberFormatter::formatOrderNumber($this->order->getId())
        );
    }

    private function triggerAction(): void
    {
        $this->markOrderAsPaid->handle($this->notificationResult);
        $this->order->refresh();
    }
}
