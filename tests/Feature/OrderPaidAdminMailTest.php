<?php

namespace Bittacora\Bpanel4\Payment\Redsys\Tests\Feature;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Payment\Redsys\Mail\OrderPaidAdminMail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ReflectionException;
use Tests\TestCase;

final class OrderPaidAdminMailTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws ReflectionException
     */
    public function testElEmailSeGeneraConLosDatosDelPedido(): void
    {
        $order = (new OrderFactory())->getFullOrder();

        /** @var OrderPaidAdminMail $mail */
        $mail = $this->app->make(OrderPaidAdminMail::class, [
            'order' => $order,
        ]);

        $mailContent = $mail->render();
        self::assertStringContainsString($order->getId(), $mailContent);
        self::assertStringContainsString('Se ha recibido el pago por tarjeta', $mailContent);
    }
}
