<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Redsys\Tests\Feature;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Payment\Contracts\OrderDetailsDto;
use Bittacora\Bpanel4\Payment\Redsys\PaymentMethods\Redsys;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

final class RedsysPaymentMethodTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws InvalidPriceException
     */
    public function testRedirigeAlFormularioDelTpv(): void
    {
        Artisan::call('bpanel4-orders:create-default-order-statuses');
        $order = (new OrderFactory())->getFullOrder();
        /** @var Redsys $redsys */
        $redsys = $this->app->make(Redsys::class);

        $result = $redsys->processPayment(
            new OrderDetailsDto(
                $order->getId(),
                Price::fromInt($order->getOrderTotal()),
                ''
            )
        );

        self::assertStringContainsString('pago-tarjeta/pedido/1', $result->getTargetUrl());
    }
}
