<?php

namespace Bittacora\Bpanel4\Payment\Redsys\Tests\Feature;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Enums\OrderStatus;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Payment\Redsys\Actions\MarkOrderAsPaymentError;
use Bittacora\Bpanel4\Payment\Redsys\Mail\OrderPaymentErrorAdminMail;
use Bittacora\Laravel\Redsys\Dtos\TpvNotificationResult;
use Bittacora\Laravel\Redsys\Services\OrderNumberFormatter;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Bittacora\Bpanel4\Payment\Redsys\Mail\OrderPaymentErrorClientMail;

class MarkOrderAsPaymentErrorTest extends TestCase
{
    use RefreshDatabase;

    private TpvNotificationResult $notificationResult;
    private MarkOrderAsPaymentError $markOrderAsPaymentError;
    private Order $order;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setNotificationResult();
        Mail::fake();
        $this->markOrderAsPaymentError = $this->app->make(MarkOrderAsPaymentError::class);
        Artisan::call('bpanel4-orders:create-default-order-statuses');
    }

    public function testMarcaElPedidoComoErrorEnElPago(): void
    {
        $this->triggerAction();
        self::assertEquals(OrderStatus::PAYMENT_ERROR->value, $this->order->getStatus()->id);
    }

    public function testEnviaUnEmailAlAdministrador(): void
    {
        $this->triggerAction();
        Mail::assertSent(OrderPaymentErrorAdminMail::class);
    }

    public function testEnviaUnEmailAlCliente(): void
    {
        $this->triggerAction();
        Mail::assertSent(OrderPaymentErrorAdminMail::class);
    }

    /**
     * @return void
     */
    private function setNotificationResult(): void
    {
        /** @var Order $order */
        $this->order = (new OrderFactory())->getFullOrder();

        $this->notificationResult = new TpvNotificationResult(
            false,
            0,
            OrderNumberFormatter::formatOrderNumber($this->order->getId())
        );
    }

    /**
     * @return void
     */
    private function triggerAction(): void
    {
        $this->markOrderAsPaymentError->handle($this->notificationResult);
        $this->order->refresh();
    }
}
