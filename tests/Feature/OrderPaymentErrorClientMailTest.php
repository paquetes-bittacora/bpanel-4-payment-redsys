<?php

namespace Bittacora\Bpanel4\Payment\Redsys\Tests\Feature;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Payment\Redsys\Mail\OrderPaymentErrorClientMail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class OrderPaymentErrorClientMailTest extends TestCase
{
    use RefreshDatabase;

    public function testElEmailSeGeneraConLosDatosDelPedido(): void
    {
        $order = (new OrderFactory())->getFullOrder();
        $mail = $this->app->make(OrderPaymentErrorClientMail::class, [
            'order' => $order,
        ]);

        $mailContent = $mail->render();
        self::assertStringContainsString($order->getId(), $mailContent);
        self::assertStringContainsString($order->getClient()->getName(), $mailContent);
        self::assertStringContainsString('Error en el pago de su pedido', $mailContent);
    }
}
