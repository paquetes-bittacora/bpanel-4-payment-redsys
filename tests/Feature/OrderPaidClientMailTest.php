<?php

namespace Bittacora\Bpanel4\Payment\Redsys\Tests\Feature;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Payment\Redsys\Mail\OrderPaidClientMail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ReflectionException;
use Tests\TestCase;

final class OrderPaidClientMailTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws ReflectionException
     */
    public function testElEmailSeGeneraConLosDatosDelPedido(): void
    {
        $order = (new OrderFactory())->getFullOrder();

        /** @var OrderPaidClientMail $mail */
        $mail = $this->app->make(OrderPaidClientMail::class, [
            'order' => $order,
        ]);

        $mailContent = $mail->render();
        self::assertStringContainsString($order->getId(), $mailContent);
        self::assertStringContainsString($order->getClient()->getName(), $mailContent);
        self::assertStringContainsString('Pago confirmado', $mailContent);
    }
}
