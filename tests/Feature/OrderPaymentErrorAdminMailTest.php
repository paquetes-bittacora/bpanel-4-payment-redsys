<?php

namespace Bittacora\Bpanel4\Payment\Redsys\Tests\Feature;

use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Payment\Redsys\Mail\OrderPaymentErrorAdminMail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ReflectionException;
use Tests\TestCase;

final class OrderPaymentErrorAdminMailTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws ReflectionException
     */
    public function testElEmailSeGeneraConLosDatosDelPedido(): void
    {
        $order = (new OrderFactory())->getFullOrder();

        /** @var OrderPaymentErrorAdminMail $mail */
        $mail = $this->app->make(OrderPaymentErrorAdminMail::class, [
            'order' => $order,
        ]);

        $mailContent = $mail->render();
        self::assertStringContainsString($order->getId(), $mailContent);
        self::assertStringContainsString('Error en el pago del pedido', $mailContent);
    }
}
