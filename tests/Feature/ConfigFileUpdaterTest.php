<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Redsys\Tests\Feature;

use Bittacora\Bpanel4\Payment\Redsys\Services\ConfigFileUpdater;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\UrlGenerator;
use Tests\TestCase;

final class ConfigFileUpdaterTest extends TestCase
{
    private const TEST_FILES_DIR = __DIR__ . DIRECTORY_SEPARATOR . 'test-files';
    private const TEST_CONFIG_FILE = self::TEST_FILES_DIR . '/config/redsys.php';
    private UrlGenerator $urlGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->urlGenerator = $this->app->make(UrlGenerator::class);
    }

    public function testAnadeLaUrlOkYKo(): void
    {
        $configUpdater = $this->getConfigUpdater();

        $configUpdater->setUrlOkAndKo();

        $result = file_get_contents(self::TEST_CONFIG_FILE);
        self::assertStringContainsString($this->urlGenerator->route('bpanel4-redsys.url-ok'), $result);
        self::assertStringContainsString($this->urlGenerator->route('bpanel4-redsys.url-ko'), $result);
        $this->removeTestFile();
    }

    public function testAnadeLaUrlDeNotificacion(): void
    {
        $configUpdater = $this->getConfigUpdater();

        $configUpdater->setNotificationUrl();

        $result = file_get_contents(self::TEST_CONFIG_FILE);
        self::assertStringContainsString($this->urlGenerator->route('bpanel4-redsys.url-notification'), $result);
        $this->removeTestFile();
    }

    private function getConfigUpdater(): ConfigFileUpdater
    {
        /** @var Application $application */
        $application = $this->app->make(Application::class);
        $configUpdater = new ConfigFileUpdater($application, $this->urlGenerator, self::TEST_FILES_DIR);

        copy(self::TEST_FILES_DIR . '/config/default-config', self::TEST_CONFIG_FILE);
        return $configUpdater;
    }

    private function removeTestFile(): void
    {
        unlink(self::TEST_CONFIG_FILE);
    }
}
