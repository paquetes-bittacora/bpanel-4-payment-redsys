<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Redsys\Commands;

use Bittacora\Bpanel4\Payment\DomiciledReceipt\PaymentMethods\DomiciledReceipt;
use Bittacora\Bpanel4\Payment\Exceptions\PaymentMethodAlreadyRegisteredException;
use Bittacora\Bpanel4\Payment\Payment;
use Bittacora\Bpanel4\Payment\Redsys\PaymentMethods\Redsys;
use Bittacora\Bpanel4\Payment\Redsys\Services\ConfigFileUpdater;
use Illuminate\Console\Command;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;

final class InstallCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'bpanel4-redsys:install';
    /**
     * @var string
     */
    protected $description = 'Registra las opciones de pago por tarjeta (redsys).';

    /**
     * @throws PaymentMethodAlreadyRegisteredException|BindingResolutionException
     */
    public function handle(
        Container $container,
        ConfigFileUpdater $configFileUpdater,
    ): void {
        $this->registerPaymentMethod($container);
        $this->registerUrls($configFileUpdater);
    }

    /**
     * @throws PaymentMethodAlreadyRegisteredException
     * @throws BindingResolutionException
     */
    private function registerPaymentMethod(Container $container): void
    {
        $this->comment('Registrando método de pago');
        /** @var Payment $paymentModule */
        $paymentModule = $container->make(Payment::class);
        /** @var DomiciledReceipt $paymentMethod */
        $paymentMethod = $container->make(Redsys::class);
        $paymentModule->registerPaymentMethod($paymentMethod);
    }

    private function registerUrls(ConfigFileUpdater $configFileUpdater): void
    {
        $this->comment('Registrando URLs');
        $configFileUpdater->setUrlOkAndKo();
        $configFileUpdater->setNotificationUrl();
    }
}
