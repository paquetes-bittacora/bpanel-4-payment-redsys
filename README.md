# bPanel 4 Redsys

Registra rutas (por ejemplo URL OK y KO, url de notificación del TPV, etc), método de pago, etc de Redsys para bPanel 4.

Es una capa para incorporar bittacora/laravel-redsys a bPanel 4 automáticamente. Ver la documentación de ese paquete para más información.

**Importante**: Debe rellenarse el archivo config/redsys.php para añadir los datos del comercio.
