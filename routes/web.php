<?php

declare(strict_types=1);


use App\Http\Middleware\VerifyCsrfToken;
use Bittacora\Bpanel4\Payment\Redsys\Http\Controllers\RedsysPaymentController;
use Illuminate\Support\Facades\Route;

Route::prefix('/pago-tarjeta')->name('bpanel4-redsys.')->middleware(['web'])->group(function () {
    Route::get('/pedido/{order}', [RedsysPaymentController::class, 'showForm'])->name('show-form');
    Route::get('/error-pago', [RedsysPaymentController::class, 'showUrlKo'])->name('url-ko');
    Route::get('/pago-ok', [RedsysPaymentController::class, 'showUrlOk'])->name('url-ok');
    Route::post('/notificacion-tpv', [RedsysPaymentController::class, 'processTpvNotification'])
        ->name('url-notification')->withoutMiddleware([VerifyCsrfToken::class]);
});
